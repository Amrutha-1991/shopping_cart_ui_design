import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { google } from "google-maps";


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit,AfterViewInit {
  @Input() adressType: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('addresstext') addresstext: any;
  
  autocompleteInput: string;
  queryWait: boolean;
  

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.getPlaceAutocomplete();
}
private getPlaceAutocomplete() {
   var google : google;
  const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
      {
          componentRestrictions: { country: 'US' },
          types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
      });
  google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      this.invokeEvent(place);
  });
}

invokeEvent(place: Object) {
  this.setAddress.emit(place);
}

}
